package com.hw.db.controllers;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.stubbing.Answer;

import com.hw.db.DAO.ForumDAO;
import com.hw.db.DAO.ThreadDAO;
import com.hw.db.DAO.UserDAO;
import com.hw.db.models.Forum;
import com.hw.db.models.Post;
import com.hw.db.models.User;
import com.hw.db.models.Thread;
import com.hw.db.models.Vote;
import com.hw.db.controllers.ThreadController;

import org.springframework.dao.DataRetrievalFailureException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class ThreadControllerTests {
    private DataRetrievalFailureException sampleException;
    private String sampleSlug;
    private String sampleId;
    private String sampleAuthor;
    private String sampleForum;
    private String sampleMessage;
    private Integer sampleVotes;
    private String sampleTitle;
    private Thread sampleThread;
    private Post samplePost;
    private User sampleUser;

    @BeforeEach
    void setup() {
        sampleException = new DataRetrievalFailureException("");
        sampleSlug = "aboba_slug";
        sampleId = "31337";
        sampleAuthor = "gladValakas";
        sampleForum = "coolForum";
        sampleMessage = "ya bolgarkoi navbuk raspilil; nichego strashnogo?";
        sampleTitle = "bolgarskij navbuk";
        sampleVotes = 54;
        sampleThread = new Thread(
                sampleAuthor,
                Timestamp.from(Instant.MIN),
                sampleForum,
                sampleMessage,
                sampleSlug,
                sampleTitle,
                54);
        // To the person, who didn't put id in constructor:
        //
        // I wish you will always have NullPointerException
        // that occurs randomly and is not debuggable.
        sampleThread.setId(Integer.parseInt(sampleId));
        samplePost = new Post();
        samplePost.setAuthor(sampleAuthor);
        sampleUser = new User();
        sampleUser.setNickname(sampleAuthor);
    }

    void setupThreadMock(MockedStatic<ThreadDAO> threadMock) {
        threadMock.when(() -> ThreadDAO.getThreadBySlug(anyString()))
                .thenThrow(sampleException);
        threadMock.when(() -> ThreadDAO.getThreadBySlug(sampleSlug))
                .thenReturn(sampleThread);
        threadMock.when(() -> ThreadDAO.getThreadById(anyInt()))
                .thenThrow(sampleException);
        threadMock.when(() -> ThreadDAO.getThreadById(Integer.parseInt(sampleId)))
                .thenReturn(sampleThread);
    }

    void setupUserMock(MockedStatic<UserDAO> userMock) {
        userMock.when(() -> UserDAO.Info(anyString()))
                .thenThrow(sampleException);
        userMock.when(() -> UserDAO.Info(sampleAuthor))
                .thenReturn(sampleUser);
    }

    @Nested
    class ThreadRetrieval {
        @Test
        @DisplayName("Successful retrieves, no errors")
        void normalOperation() {
            try (MockedStatic<UserDAO> userMock = Mockito.mockStatic(UserDAO.class)) {
                try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
                    setupThreadMock(threadMock);
                    ThreadController controller = new ThreadController();
                    Thread thread_from_string = controller.CheckIdOrSlug(sampleSlug);
                    assertEquals(sampleThread,
                            controller.CheckIdOrSlug(sampleSlug),
                            "Result for succeeding forum creation");
                    Thread thread_from_id = controller.CheckIdOrSlug(sampleId);
                    assertEquals(sampleThread,
                            controller.CheckIdOrSlug(sampleId),
                            "Result for succeeding forum creation");
                }
            }
        }

        @Test
        @DisplayName("Expected errors")
        void errorHandling() {
            try (MockedStatic<UserDAO> userMock = Mockito.mockStatic(UserDAO.class)) {
                try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
                    setupThreadMock(threadMock);
                    ThreadController controller = new ThreadController();
                    Thread thread_from_string = controller.CheckIdOrSlug(sampleSlug);
                    assertThrows(
                            DataRetrievalFailureException.class,
                            () -> controller.CheckIdOrSlug("keka"));
                    assertThrows(
                            DataRetrievalFailureException.class,
                            () -> controller.CheckIdOrSlug("12345"));
                }
            }
        }

    }

    @Nested
    class PostCreation {
        @Test
        @DisplayName("Correct thread creation test")
        void normalOperation() {
            List<Post> posts = new ArrayList<Post>();
            posts.add(samplePost);
            try (MockedStatic<UserDAO> userMock = Mockito.mockStatic(UserDAO.class)) {
                setupUserMock(userMock);
                try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
                    setupThreadMock(threadMock);
                    ThreadController controller = new ThreadController();

                    ResponseEntity response = controller.createPost(sampleSlug, posts);
                    assertEquals(response.getStatusCode(), HttpStatus.CREATED);
                    assertEquals(response.getBody(), posts);

                    ResponseEntity response2 = controller.createPost(sampleSlug, new ArrayList<Post>());
                    assertEquals(response2.getStatusCode(), HttpStatus.CREATED);
                    assertEquals(response2.getBody(), new ArrayList<Post>());
                }
            }
        }

        @Nested
        class Errors {
            @Test
            @DisplayName("user unknown")
            void unknownUser() {
                List<Post> posts = new ArrayList<Post>();
                posts.add(samplePost);
                try (MockedStatic<UserDAO> userMock = Mockito.mockStatic(UserDAO.class)) {
                    userMock.when(() -> UserDAO.Info(anyString()))
                            .thenThrow(sampleException);
                    try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
                        setupThreadMock(threadMock);
                        ThreadController controller = new ThreadController();

                        ResponseEntity response = controller.createPost(sampleSlug, posts);
                        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
                    }
                }
            }

            @Test
            @DisplayName("thread unknown")
            void unknownThread() {
                List<Post> posts = new ArrayList<Post>();
                posts.add(samplePost);
                try (MockedStatic<UserDAO> userMock = Mockito.mockStatic(UserDAO.class)) {
                    setupUserMock(userMock);
                    try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
                        setupThreadMock(threadMock);
                        ThreadController controller = new ThreadController();

                        ResponseEntity response = controller.createPost("unknown slug", posts);
                        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
                        ResponseEntity response2 = controller.createPost("123456", posts);
                        assertEquals(HttpStatus.NOT_FOUND, response2.getStatusCode());
                    }
                }
            }

            @Test
            @DisplayName("incorrect parent")
            void parentOtherCastle() {
                List<Post> posts = new ArrayList<Post>();
                posts.add(samplePost);
                try (MockedStatic<UserDAO> userMock = Mockito.mockStatic(UserDAO.class)) {
                    setupUserMock(userMock);
                    try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
                        setupThreadMock(threadMock);
                        threadMock.when(() -> ThreadDAO.createPosts(any(), any(), any())).thenThrow(sampleException);
                        ThreadController controller = new ThreadController();

                        ResponseEntity response = controller.createPost(sampleSlug, posts);
                        assertEquals(HttpStatus.CONFLICT, response.getStatusCode());
                    }
                }
            }
        }
    }

    @Nested
    class PostRetrieval {
        @Test
        @DisplayName("Correct post retrieval test")
        void normalOperation() {
            List<Post> posts = new ArrayList<Post>();
            posts.add(samplePost);
            try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
                setupThreadMock(threadMock);
                threadMock.when(() -> ThreadDAO.getPosts(any(), any(), any(), any(), any()))
                        .thenReturn(posts);
                ThreadController controller = new ThreadController();

                ResponseEntity response = controller.Posts(sampleSlug, 0, 0, null, false);
                assertEquals(HttpStatus.OK, response.getStatusCode());
                assertEquals(posts, response.getBody());
            }
        }

        @Test
        @DisplayName("thread unknown")
        void unknownThread() {
            List<Post> posts = new ArrayList<Post>();
            posts.add(samplePost);
            try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
                setupThreadMock(threadMock);
                ThreadController controller = new ThreadController();

                ResponseEntity response = controller.Posts("21345678", 0, 0, null, false);
                assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
            }
        }
    }

    @Nested
    class PostDetailsChange {
        @Test
        @DisplayName("Correct details update test")
        void normalOperation() {
            try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
                setupThreadMock(threadMock);
                threadMock
                        .when(() -> ThreadDAO.change(sampleThread, sampleThread))
                        .thenAnswer((Answer<Void>) invocation -> null);

                ThreadController controller = new ThreadController();

                ResponseEntity response = controller.change(sampleId, sampleThread);
                assertEquals(HttpStatus.OK, response.getStatusCode());
                assertEquals(sampleThread, response.getBody());
            }
        }

        @Test
        @DisplayName("thread unknown")
        void unknownThread() {
            try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
                setupThreadMock(threadMock);
                ThreadController controller = new ThreadController();

                ResponseEntity response = controller.change("21345678", sampleThread);
                assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
            }
        }
    }

    @Nested
    class PostDetailsGet {
        @Test
        @DisplayName("Correct details get test")
        void normalOperation() {
            try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
                setupThreadMock(threadMock);

                ThreadController controller = new ThreadController();

                ResponseEntity response = controller.info(sampleId);
                assertEquals(HttpStatus.OK, response.getStatusCode());
                assertEquals(sampleThread, response.getBody());
            }
        }

        @Test
        @DisplayName("thread unknown")
        void unknownThread() {
            try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
                setupThreadMock(threadMock);
                ThreadController controller = new ThreadController();

                ResponseEntity response = controller.info("21345678");
                assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
            }
        }
    }

    @Nested
    class Voting {
        @Test
        @DisplayName("Correct voting test")
        void normalOperation() {
            try (MockedStatic<UserDAO> userMock = Mockito.mockStatic(UserDAO.class)) {
                setupUserMock(userMock);
                try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
                    setupThreadMock(threadMock);
                    ThreadController controller = new ThreadController();
                    Vote vote = new Vote(sampleAuthor, 228);
                    threadMock.when(() -> ThreadDAO.change(vote, 54 + 228)).thenReturn(54 + 228);

                    ResponseEntity response = controller.createVote(sampleSlug, vote);
                    assertEquals(HttpStatus.OK, response.getStatusCode());
                    int new_votes = sampleThread.getVotes();
                    assertEquals(new_votes, 54 + 228);
                    assertEquals(sampleThread, response.getBody());
                }
            }
        }

        @Test
        @DisplayName("unknown thread")
        void unknownThread() {
            try (MockedStatic<UserDAO> userMock = Mockito.mockStatic(UserDAO.class)) {
                setupUserMock(userMock);
                try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
                    setupThreadMock(threadMock);
                    ThreadController controller = new ThreadController();
                    Vote vote = new Vote(sampleAuthor, 228);
                    threadMock.when(() -> ThreadDAO.change(vote, 54 + 228)).thenReturn(54 + 228);

                    ResponseEntity response = controller.createVote("unknownSlug", vote);
                    assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
                }
            }
        }

        @Test
        @DisplayName("incorrect voice")
        void incorrectVoice() {
            try (MockedStatic<UserDAO> userMock = Mockito.mockStatic(UserDAO.class)) {
                setupUserMock(userMock);
                try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
                    setupThreadMock(threadMock);
                    ThreadController controller = new ThreadController();
                    Vote vote = new Vote(sampleAuthor, -228);
                    threadMock.when(() -> ThreadDAO.change(vote, 54 - 228)).thenReturn(54 - 228);

                    ResponseEntity response = controller.createVote(sampleSlug, vote);
                    assertEquals(HttpStatus.OK, response.getStatusCode());
                    int new_votes = sampleThread.getVotes();
                    // shouldn't be this way, but our task was to write tests that pass not to fix
                    // the code
                    assertEquals(new_votes, 54 - 228);
                    assertEquals(sampleThread, response.getBody());
                }
            }
        }

        @Test
        @DisplayName("unknown vote author")
        void unknownAuthor() {
            try (MockedStatic<UserDAO> userMock = Mockito.mockStatic(UserDAO.class)) {
                setupUserMock(userMock);
                try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
                    setupThreadMock(threadMock);
                    ThreadController controller = new ThreadController();
                    Vote vote = new Vote("unknownAuthor", 228);
                    threadMock.when(() -> ThreadDAO.change(vote, 54 + 228)).thenReturn(54 + 228);

                    ResponseEntity response = controller.createVote(sampleSlug, vote);
                    assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
                }
            }
        }
    }
}
